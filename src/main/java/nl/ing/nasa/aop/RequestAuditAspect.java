package nl.ing.nasa.aop;


import nl.ing.nasa.domain.RequestAudit;
import nl.ing.nasa.service.RequestAuditService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import java.time.Instant;

@Aspect
@Configuration
public class RequestAuditAspect {

    private final static Logger LOGGER = LoggerFactory.getLogger(RequestAuditAspect.class);

    private RequestAuditService auditService;

    public RequestAuditAspect(RequestAuditService auditService) {
        this.auditService = auditService;
    }

    /**
     * This advice makes a proxy around any method which has the <code>@LogResponse</code> annotation and calculates the
     * execution time of that method and creates a <code>RequestAudit</code> and persist it to database.
     *
     * @param joinPoint The desired joinPoint
     * @return The result of the executed method
     * @throws Throwable Happened error
     */
    @Around("@annotation(nl.ing.nasa.aop.LogResponseTime)")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {

        long startTime = System.currentTimeMillis();
        Object result = joinPoint.proceed();
        long responseTime = System.currentTimeMillis() - startTime;

        LOGGER.info("Time Taken by {} is {}", joinPoint, responseTime);

        RequestAudit requestAudit = new RequestAudit();
        requestAudit.setCreatedDate(Instant.now());
        requestAudit.setResponseTime(responseTime);
        String methodName = ((MethodSignature) joinPoint.getSignature()).getMethod().getName();
        requestAudit.setRequestedMethodName(methodName);
        auditService.saveAudit(requestAudit);

        return result;
    }
}
