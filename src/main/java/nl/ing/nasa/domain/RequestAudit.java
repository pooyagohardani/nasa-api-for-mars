package nl.ing.nasa.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

/**
 * This class is an entity to store auditing information of a request
 */

@Entity
@Table(name = "tbl_request_audit")
public class RequestAudit implements Serializable {

    private static final long serialVersionUID = -5124551903924032376L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String requestedMethodName;

    private long responseTime;

    private Instant createdDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRequestedMethodName() {
        return requestedMethodName;
    }

    public void setRequestedMethodName(String requestedMethodName) {
        this.requestedMethodName = requestedMethodName;
    }

    public long getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(long responseTime) {
        this.responseTime = responseTime;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RequestAudit that = (RequestAudit) o;

        return id == that.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
