package nl.ing.nasa.interceptor;

import nl.ing.nasa.config.ConfigProperties;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.support.HttpRequestWrapper;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URI;

/**
 * This class provides an interceptor to add api-key to any outgoing request to for the external API
 */

public class ApiKeyInterceptor implements ClientHttpRequestInterceptor {

    private ConfigProperties configProperties;

    public ApiKeyInterceptor(ConfigProperties configProperties) {
        this.configProperties = configProperties;
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
            throws IOException {

        URI uri = UriComponentsBuilder.fromHttpRequest(request)
                .queryParam("api_key", configProperties.getApiKey())
                .build().toUri();

        HttpRequest modifiedRequest = new HttpRequestWrapper(request) {
            @Override
            public URI getURI() {
                return uri;
            }
        };
        return execution.execute(modifiedRequest, body);
    }
}
