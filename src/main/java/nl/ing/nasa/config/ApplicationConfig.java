package nl.ing.nasa.config;

import nl.ing.nasa.NasaApplication;
import nl.ing.nasa.interceptor.ApiKeyInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ApplicationConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(NasaApplication.class);

    private ConfigProperties configProperties;

    public ApplicationConfig(ConfigProperties configProperties) {
        this.configProperties = configProperties;
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        LOGGER.info("Preparing restTemplate...");
        return builder
                .interceptors(new ApiKeyInterceptor(configProperties))
                .build();
    }
}
