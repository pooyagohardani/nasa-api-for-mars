package nl.ing.nasa.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * This configuration class helps to prevent of using @Value in the code, so one can simply inject it to wherever is needed
 */

@Configuration
@PropertySource("classpath:application.properties")
@ConfigurationProperties(prefix = "nasa")
public class ConfigProperties {

    private String apiUrl;
    private String apiKey;


    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
