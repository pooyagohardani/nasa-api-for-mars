package nl.ing.nasa.repository;

import nl.ing.nasa.domain.RequestAudit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestAuditRepository extends JpaRepository<RequestAudit, Long> {
}
