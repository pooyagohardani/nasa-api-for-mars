package nl.ing.nasa.web.rest;

import nl.ing.nasa.service.PhotoService;
import nl.ing.nasa.web.rest.dto.CameraEnum;
import nl.ing.nasa.web.rest.dto.PhotoManifest;
import nl.ing.nasa.web.rest.dto.PhotosDto;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api")
@Validated
public class PhotoResource {

    private PhotoService photoService;

    public PhotoResource(PhotoService photoService) {
        this.photoService = photoService;
    }

    @GetMapping("/photos-by-sol")
    public PhotosDto fetchPhotosBySol(
            @NotEmpty @RequestParam("rover_name") String roverName,
            @NotNull @RequestParam Integer sol,
            @RequestParam CameraEnum camera,
            @RequestParam Integer page) {

        return photoService.fetchPhotosBySol(roverName, sol, camera, page);
    }

    @GetMapping("/photos-by-earth-date")
    public PhotosDto fetchPhotosByEarthDate(
            @NotEmpty @RequestParam("rover_name") String roverName,
            @NotEmpty @RequestParam("earth_date") String earthDate,
            @RequestParam CameraEnum camera,
            @RequestParam Integer page) {

        return photoService.fetchPhotosByEarthDate(roverName, earthDate, camera, page);
    }

    @GetMapping("/mission-manifest/{rover_name}")
    public PhotoManifest fetchMissionManifest(@NotEmpty @PathVariable("rover_name") String roverName) {
        return photoService.fetchMissionManifest(roverName);
    }

    @GetMapping("/camera-list")
    public CameraEnum[] getCameras() {
        return CameraEnum.values();
    }

}
