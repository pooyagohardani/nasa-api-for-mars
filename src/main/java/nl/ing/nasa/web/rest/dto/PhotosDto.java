package nl.ing.nasa.web.rest.dto;

import java.util.List;

public class PhotosDto {
    private List<PhotoDto> photos;

    public List<PhotoDto> getPhotos() {
        return photos;
    }

    public void setPhotos(List<PhotoDto> photos) {
        this.photos = photos;
    }
}
