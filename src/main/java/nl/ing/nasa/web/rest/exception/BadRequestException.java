package nl.ing.nasa.web.rest.exception;

public class BadRequestException extends RuntimeException {
    private static final long serialVersionUID = 6839171287487093763L;

    public BadRequestException() {
    }

    public BadRequestException(String message) {
        super(message);
    }
}
