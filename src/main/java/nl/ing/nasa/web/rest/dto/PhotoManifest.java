package nl.ing.nasa.web.rest.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.time.LocalDate;
import java.util.List;

@JsonTypeName("photo_manifest")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class PhotoManifest {

    private long id;

    private String name;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonProperty("landing_date")
    private LocalDate landingDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonProperty("launch_date")
    private LocalDate launchDate;

    private String status;

    @JsonProperty("max_sol")
    private int sol;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonProperty("max_date")
    private LocalDate maxDate;

    @JsonProperty("total_photos")
    private int totalPhotos;

    private List<ManifestPhotoDto> photos;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getLandingDate() {
        return landingDate;
    }

    public void setLandingDate(LocalDate landingDate) {
        this.landingDate = landingDate;
    }

    public LocalDate getLaunchDate() {
        return launchDate;
    }

    public void setLaunchDate(LocalDate launchDate) {
        this.launchDate = launchDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getSol() {
        return sol;
    }

    public void setSol(int sol) {
        this.sol = sol;
    }

    public LocalDate getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(LocalDate maxDate) {
        this.maxDate = maxDate;
    }

    public int getTotalPhotos() {
        return totalPhotos;
    }

    public void setTotalPhotos(int totalPhotos) {
        this.totalPhotos = totalPhotos;
    }

    public List<ManifestPhotoDto> getPhotos() {
        return photos;
    }

    public void setPhotos(List<ManifestPhotoDto> photos) {
        this.photos = photos;
    }
}
