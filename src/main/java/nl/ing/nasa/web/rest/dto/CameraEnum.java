package nl.ing.nasa.web.rest.dto;

public enum CameraEnum {
    ALL("", false, false, false),
    FHAZ("Front Hazard Avoidance CameraDto", true, true, true),
    RHAZ("Rear Hazard Avoidance CameraDto", true, true, true),
    MAST("Mast CameraDto", true, false, false),
    CHEMCAM("Chemistry and CameraDto Complex", true, false, false),
    MAHLI("Mars Hand Lens Imager", true, false, false),
    MARDI("Mars Descent Imager", true, false, false),
    NAVCAM("Navigation CameraDto", true, true, true),
    PANCAM("Panoramic CameraDto", false, true, true),
    MINITES("Miniature Thermal Emission Spectrometer (Mini-TES)", false, true, true);

    private String fullName;
    private boolean curiousity;
    private boolean opportunity;
    private boolean spirit;

    CameraEnum(String fullName, boolean curiousity, boolean opportunity, boolean spirit) {
        this.fullName = fullName;
        this.curiousity = curiousity;
        this.opportunity = opportunity;
        this.spirit = spirit;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public boolean isCuriousity() {
        return curiousity;
    }

    public void setCuriousity(boolean curiousity) {
        this.curiousity = curiousity;
    }

    public boolean isOpportunity() {
        return opportunity;
    }

    public void setOpportunity(boolean opportunity) {
        this.opportunity = opportunity;
    }

    public boolean isSpirit() {
        return spirit;
    }

    public void setSpirit(boolean spirit) {
        this.spirit = spirit;
    }
}
