package nl.ing.nasa.web.rest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.util.List;

public class ManifestPhotoDto {

    public ManifestPhotoDto() {
    }

    private int sol;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonProperty("earth_date")
    private LocalDate earthDate;

    @JsonProperty("total_photos")
    private int totalPhotos;

    private List<CameraEnum> cameras;

    public int getSol() {
        return sol;
    }

    public void setSol(int sol) {
        this.sol = sol;
    }

    public LocalDate getEarthDate() {
        return earthDate;
    }

    public void setEarthDate(LocalDate earthDate) {
        this.earthDate = earthDate;
    }

    public int getTotalPhotos() {
        return totalPhotos;
    }

    public void setTotalPhotos(int totalPhotos) {
        this.totalPhotos = totalPhotos;
    }

    public List<CameraEnum> getCameras() {
        return cameras;
    }

    public void setCameras(List<CameraEnum> cameras) {
        this.cameras = cameras;
    }
}
