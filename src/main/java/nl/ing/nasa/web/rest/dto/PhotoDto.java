package nl.ing.nasa.web.rest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

public class PhotoDto {

    private long id;
    private int sol;
    private CameraDto camera;

    @JsonProperty("img_src")
    private String imageSrc;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonProperty("earth_date")
    private LocalDate earthDate;

    private RoverDto rover;

    public PhotoDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getSol() {
        return sol;
    }

    public void setSol(int sol) {
        this.sol = sol;
    }

    public CameraDto getCamera() {
        return camera;
    }

    public void setCamera(CameraDto camera) {
        this.camera = camera;
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public void setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
    }

    public LocalDate getEarthDate() {
        return earthDate;
    }

    public void setEarthDate(LocalDate earthDate) {
        this.earthDate = earthDate;
    }

    public RoverDto getRover() {
        return rover;
    }

    public void setRover(RoverDto rover) {
        this.rover = rover;
    }

}
