package nl.ing.nasa.service;

import nl.ing.nasa.domain.RequestAudit;
import nl.ing.nasa.repository.RequestAuditRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RequestAuditService {

    private final Logger LOGGER = LoggerFactory.getLogger(RequestAuditService.class);

    private RequestAuditRepository repository;

    public RequestAuditService(RequestAuditRepository repository) {
        this.repository = repository;
    }

    public RequestAudit saveAudit(RequestAudit requestAudit) {
        LOGGER.info("Saving requestAudit object to database. {}", requestAudit);
        return repository.save(requestAudit);
    }

}
