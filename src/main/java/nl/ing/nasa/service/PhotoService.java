package nl.ing.nasa.service;


import nl.ing.nasa.aop.LogResponseTime;
import nl.ing.nasa.config.ConfigProperties;
import nl.ing.nasa.web.rest.dto.CameraEnum;
import nl.ing.nasa.web.rest.dto.PhotoManifest;
import nl.ing.nasa.web.rest.dto.PhotosDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * This is a service to interact an external API to fetch some photos based on incoming parameters
 */

@Service
@Transactional
public class PhotoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PhotoService.class);


    private ConfigProperties configProperties;

    private RestTemplate restTemplate;

    public PhotoService(ConfigProperties configProperties, RestTemplate restTemplate) {
        this.configProperties = configProperties;
        this.restTemplate = restTemplate;
    }

    /**
     * This method accepts following parameters  considering Sol and makes a REST call to the defined api-url in application.properties
     * and return an object of <code>{@link PhotosDto}</code> which is a wrapper for the list of <code>PhotoDto</code>
     *
     * @param roverName Name of the rover
     * @param sol       Sol (ranges from 0 to max found in endpoint)
     * @param camera    Rover camera name
     * @param page      Page number
     * @return An object of <code>PhotosDto</code>
     */
    @LogResponseTime
    public PhotosDto fetchPhotosBySol(String roverName, Integer sol, CameraEnum camera, Integer page) {

        LOGGER.info("Fetch photos by Sol is calling...");

        PhotosDto response;
        String transactionUrl = configProperties.getApiUrl() + "/rovers/" + roverName + "/photos";

        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString(transactionUrl)
                // Add query parameter
                .queryParam("sol", sol);
        if (camera != null) {
            builder = builder.queryParam("camera", camera);
        }
        if (page != null) {
            builder = builder.queryParam("page", page);
        }
        response = restTemplate.getForObject(builder.toUriString(), PhotosDto.class);

        return response;
    }


    /**
     * This method accepts following parameters considering <code>earthDate</code> and makes a REST call to the defined api-url in application.properties
     * and return an object of <code>{@link PhotosDto}</code> which is a wrapper for the list of <code>PhotoDto</code>
     *
     * @param roverName Name of the rover
     * @param earthDate Earth date of the taken photo
     * @param camera    Rover camera name
     * @param page      Page number
     * @return An object of <code>PhotosDto</code>
     */
    @LogResponseTime
    public PhotosDto fetchPhotosByEarthDate(String roverName, String earthDate, CameraEnum camera, Integer page) {

        LOGGER.info("Fetch photos by Earth Date is calling...");

        String transactionUrl = configProperties.getApiUrl() + "/rovers/" + roverName + "/photos";

        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString(transactionUrl);
        // Add query parameter
        builder.queryParam("earth_date", earthDate);
        if (camera != null) {
            builder = builder.queryParam("camera", camera);
        }
        if (page != null) {
            builder = builder.queryParam("page", page);
        }

        return restTemplate.getForObject(builder.toUriString(), PhotosDto.class);
    }

    /**
     * This method accepts <code>roverName</code> and makes a REST call to the defined api-url in application.properties
     * and return an object of <code>{@link PhotoManifest}</code> which is a wrapper for the list of <code>PhotoDto</code>
     *
     * @param roverName Name of the rover
     * @return An object of <code>{@link PhotoManifest}</code>
     */
    @LogResponseTime
    public PhotoManifest fetchMissionManifest(String roverName) {

        LOGGER.info("Fetch Mission Manifest is calling...");

        String transactionUrl = configProperties.getApiUrl() + "/manifests/" + roverName;

        return restTemplate.getForObject(transactionUrl, PhotoManifest.class);
    }


}
