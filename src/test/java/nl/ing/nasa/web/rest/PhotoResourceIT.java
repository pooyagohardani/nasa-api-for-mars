package nl.ing.nasa.web.rest;

import nl.ing.nasa.NasaApplication;
import nl.ing.nasa.domain.RequestAudit;
import nl.ing.nasa.repository.RequestAuditRepository;
import nl.ing.nasa.service.PhotoService;
import nl.ing.nasa.web.rest.dto.CameraEnum;
import nl.ing.nasa.web.rest.exception.ApplicationExceptionHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = NasaApplication.class)
public class PhotoResourceIT {


    private static final String DEFAULT_ROVER_NAME = "curiosity";
    private MockMvc mockMvc;

    @Autowired
    private RequestAuditRepository auditRepository;

    @Autowired
    private PhotoService photoService;

    @Autowired
    private ApplicationExceptionHandler exceptionTranslator;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;


    @BeforeEach
    public void setup() {
        PhotoResource userResource = new PhotoResource(photoService);

        this.mockMvc = MockMvcBuilders.standaloneSetup(userResource)
                .setControllerAdvice(exceptionTranslator)
                .setMessageConverters(jacksonMessageConverter)
                .build();
    }

    @Test
    @Transactional
    public void fetchPhotosBySol() throws Exception {
        int databaseSizeBeforeCreate = auditRepository.findAll().size();

        mockMvc.perform(get("/api/photos-by-sol")
                .param("rover_name", DEFAULT_ROVER_NAME)
                .param("sol", "1000")
                .param("camera", CameraEnum.FHAZ.name())
                .param("page", "1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        List<RequestAudit> audits = auditRepository.findAll();
        assertThat(audits).hasSize(databaseSizeBeforeCreate + 1);
        RequestAudit testAudit = audits.get(audits.size() - 1);
        assertThat(testAudit.getRequestedMethodName()).isEqualTo("fetchPhotosBySol");

    }

    @Test
    @Transactional
    public void fetchPhotosByEarthDate() throws Exception {
        int databaseSizeBeforeCreate = auditRepository.findAll().size();

        mockMvc.perform(get("/api/photos-by-earth-date")
                .param("rover_name", DEFAULT_ROVER_NAME)
                .param("earth_date", "2015-3-6")
                .param("camera", CameraEnum.FHAZ.name())
                .param("page", "1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        List<RequestAudit> audits = auditRepository.findAll();
        assertThat(audits).hasSize(databaseSizeBeforeCreate + 1);
        RequestAudit testAudit = audits.get(audits.size() - 1);
        assertThat(testAudit.getRequestedMethodName()).isEqualTo("fetchPhotosByEarthDate");
    }

    @Test
    @Transactional
    public void fetchMissionManifest() throws Exception {
        int databaseSizeBeforeCreate = auditRepository.findAll().size();

        mockMvc.perform(get("/api/mission-manifest/" + DEFAULT_ROVER_NAME)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        List<RequestAudit> audits = auditRepository.findAll();
        assertThat(audits).hasSize(databaseSizeBeforeCreate + 1);
        RequestAudit testAudit = audits.get(audits.size() - 1);
        assertThat(testAudit.getRequestedMethodName()).isEqualTo("fetchMissionManifest");
    }

}
